#include <iostream>

int main()
{
    // Change the value of a pointer
    int i = 1, j = 2;
    int *i_ptr = &i, *j_ptr = &j;
    std::cout << *i_ptr << std::endl;
    i_ptr = j_ptr;
    std::cout << *i_ptr << std::endl;

    // Change the value to which i_ptr points
    *i_ptr = 5;
    std::cout << *i_ptr << std::endl;
}
