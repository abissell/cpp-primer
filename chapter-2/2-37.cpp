#include <iostream>

int main()
{
    int a = 3, b = 4;
    decltype(a) c = a; // c is an int, equal to 3
    decltype(a = b) d = a; // d is an int& ref to a, which == 3
                           // expression a = b is NOT evaluated

    std::cout << a << std::endl; // int == 3
    std::cout << b << std::endl; // int == 4
    std::cout << c << std::endl; // int == 3
    std::cout << d << std::endl; // int& == 3

    return 0;
}
