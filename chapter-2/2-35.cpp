#include <iostream>

int main()
{
    const int i = 42; // i is of type const int
    auto j = i; // j is of type int == 42
    j = 5;
    const auto &k = i; // k is of type const int& bound to i
    k = 3; // Should be compiler error
    auto *p = &i; // p is pointer to const int
    *p = 6; // Should be compiler error
    const auto j2 = i, &k2 = i; // j2 is of type const int
                                // k2 is of type const int&
    j2 = 2; // Should be compiler error
    k2 = 3; // Should be compiler error

    return 0;
}
