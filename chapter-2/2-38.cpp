#include <iostream>

int main()
{
    const int a = 5; // const is a top-level qualifier
    auto b = a; // b is a non-const int
    b = 6;

    decltype(a) d = 2; // d is a const int
    d = 4; // compiler error

    auto &a_ref = a; // a_ref is a reference to const int (const is low-level)
    // const int &a_ref = a; // a_ref is a reference to const int
    a_ref = 0; // compiler error

    decltype((a)) a_ref_dec = 3; // a_ref_dec is also reference to const int
    a_ref_dec = 1; // compiler error

    return 0;
}
