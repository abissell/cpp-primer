#include <iostream>

int main()
{
    int i = 0, &r = i;
    auto a = r; // a is an int == 0
    const int ci = i, &cr = ci;
    auto b = ci; // b is an int == ci == 0
    auto c = cr; // c is an int (cr is an alias for ci)
    auto d = &i; // d is an int*
    auto e = &ci; // e is a const int*
    const auto f = ci; // f is a const int
    auto &g = ci; // g is a reference to const int ci
    // auto &h = 42; // error: can't bind a plain reference to literal
    const auto &j = 42; // ok: we can bind a const reference to literal

    std::cout << a << std::endl;
    a = 42; // reassigns int a to equal 42
    std::cout << a << std::endl;

    std::cout << b << std::endl;
    b = 42; // reassigns int b to equal 42
    std::cout << b << std::endl;

    std::cout << c << std::endl;
    c = 42; // reassigns int c to equal 42
    std::cout << c << std::endl;

    // d = 42; // compiler error or warning?
    std::cout << *d << std::endl;
    // e = 42; // compiler error or warning?
    std::cout << *e << std::endl;
    // g = 42; // compiler error? g is not const int&, can't bind to literal 42
    std::cout << g << std::endl;

    return 0;
}
