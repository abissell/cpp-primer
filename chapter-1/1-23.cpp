#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item input;
    Sales_item sum;
    std::cin >> sum;
    while (std::cin >> input) {
        if (sum.isbn() == input.isbn()) {
            sum += input;
        } else {
            std::cout << sum << std::endl;
            sum = input;
        }
    }
    std::cout << sum << std::endl;
    return 0;
}
