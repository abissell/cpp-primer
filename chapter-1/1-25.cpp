#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item sum;
    if (std::cin >> sum) {
        Sales_item input;
        while (std::cin >> input) {
            if (sum.isbn() == input.isbn()) {
                sum += input;
            } else {
                std::cout << sum << std::endl;
                sum = input;
            }
        }
        std::cout << sum << std::endl;
    } else {
        std::cerr << "No data?!" << std::endl;
        return -1;
    }

    return 0;
}
