#include <iostream>

int main()
{
    // Evaluate ~'q' << 6:
    // 1.) ~'q' = 10001110 = 2 + 4 + 8 + 128 = 142
    std::cout << (int) 'q' << std::endl;
    std::cout << ~'q' << " == 142?" << std::endl;
    // 2.) << 6 = 000...00010001110000000
    // 3.) = 128 + 256 + 512 + 8192 = 9088

    std::cout << (~'q' << 6) << " == 9088?" << std::endl;

    return 0;
}
