#include <iostream>

int main()
{
    unsigned long ul1 = 3, ul2 = 7;

    // 3 == 00...00011, 7 == 00...00111

    std::cout << (ul1 & ul2) << " == 3" << std::endl;
    std::cout << (ul1 | ul2) << " == 7" << std::endl;
    std::cout << (ul1 && ul2) << " == true" << std::endl;
    std::cout << (ul1 || ul2) << " == true" << std::endl;

    return 0;
}
