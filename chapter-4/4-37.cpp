#include <iostream>
#include <string>
using std::string;

int main()
{
    int i = 0;
    double d = 5.5;
    string some_str = "some_str";
    const string *ps = &some_str;
    char a_char = 'a';
    char *pc = &a_char;
    void *pv;

    pv = const_cast<void*>(reinterpret_cast<const void *>(ps));
    i = static_cast<int>(*pc);
    pv = static_cast<double*>(&d);
    pc = static_cast<char*>(pv);

    return 0;
}
