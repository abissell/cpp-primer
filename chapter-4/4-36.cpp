#include <iostream>

int main()
{
    int i = 5;
    double d = 3.0f;

    i *= static_cast<int>(d);

    std::cout << i << std::endl;
}
