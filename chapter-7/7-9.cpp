#include <iostream>
#include "Person.h"

using namespace std;

istream &read(istream &is, Person &person)
{
    is >> person.name >> person.address;
    return is;
}

ostream &print(ostream &os, const Person &person)
{
    os << person.name << " " << person.address;
    return os;
}

int main()
{

}
