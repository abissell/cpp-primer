#include <iostream>
#include "Sales_data.h"

using namespace std;

Sales_data add(const Sales_data &lhs, const Sales_data &rhs)
{
    Sales_data sum = lhs;
    sum.combine(rhs);
    return sum;
}

istream &read(istream &is, Sales_data &item)
{
    double price = 0;
    is >> item.bookNum >> item.units_sold >> price;
    item.revenue = price * item.units_sold;
    return is;
}

ostream &print(ostream &os, const Sales_data &item)
{
    os << item.isbn() << " " << item.units_sold << " "
       << item.revenue << " " << item.avg_price();
    return os;
}

int main()
{
    Sales_data sum;
    if (read(cin, sum)) {
        Sales_data input;
        while (read(cin, input)) {
            if (sum.isbn() == input.isbn()) {
                sum.combine(input);
            } else {
                print(cout, sum);
                sum = input;
            }
        }
        print(cout, sum);
    } else {
        std::cerr << "No data?!" << std::endl;
        return -1;
    }

    return 0;
}
