#ifndef SALES_DATA_H
#define SALES_DATA_H
#include <string>

struct Sales_data {
    // constructors added
    // Sales_data() = default;
    Sales_data(): units_sold(0), revenue(0.0) { };
    Sales_data(const std::string &s): bookNum(s) { }
    Sales_data(const std::string &s, unsigned n, double p):
        bookNum(s), units_sold(n), revenue(p*n) { }
    Sales_data(std::istream &);
    // new members: operations on Sales_data objects
    std::string isbn() const { return bookNum; }
    Sales_data& combine(const Sales_data&);
    double avg_price() const;
    // data members are unchanged from sec. 2.6.1
    std::string bookNum;
    unsigned units_sold = 0;
    double revenue = 0.0;
};

// nonmember Sales_data interface functions
Sales_data add(const Sales_data&, const Sales_data&);
std::ostream &print(std::ostream&, const Sales_data&);
std::istream &read(std::istream&, Sales_data&);

double Sales_data::avg_price() const 
{
    if (units_sold) {
        return revenue / units_sold;
    } else {
        return 0;
    }
}

Sales_data& Sales_data::combine(const Sales_data& rhs)
{
    units_sold += rhs.units_sold; // add the members of rhs into
    revenue += rhs.revenue;
    return *this;
}

#endif
