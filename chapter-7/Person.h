#ifndef PERSON_H
#define PERSON_H
#include <string>

struct Person {
    std::string name;
    std::string address;
    Person(const std::string &s1, const std::string &s2):
        name(s1), address(s2) { }
    std::string getName() const { return name; }
    std::string getAddress() const;
};

std::string Person::getAddress() const
{
    return address;
}

#endif
