#include <iostream>
#include <stdexcept>

int main()
{
    int input1 = 0, input2 = 0;
    bool get_input_1 = true;
    do {
        if (get_input_1) {
            std::cin >> input1;
        }
        std::cin >> input2;

        try {
            if (input2 == 0) {
                throw std::runtime_error("Can't divide by zero");
            }
            std::cout << (input1 / input2) << std::endl;
            get_input_1 = true;
        } catch (std::runtime_error e) {
            std::cout << e.what() << "\nEnter new denominator:\n";
            get_input_1 = false;
        }
    } while (std::cin);

    return 0;
}
