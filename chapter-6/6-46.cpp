#include <string>
#include <iostream>

using namespace std;

constexpr bool isShorter(const string& str1, const string& str2)
{
    return str1.size() < str2.size();
}

int main()
{
    cout << isShorter("a", "ab") << endl;
    cout << isShorter("a", "b") << endl;
    cout << isShorter("aa", "b") << endl;

    string str("abc");
    cout << isShorter("a", str) << endl;
}
