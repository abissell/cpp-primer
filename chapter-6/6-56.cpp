#include <iostream>
#include <vector>

using namespace std;

int add(int i, int j)
{
    return i + j;
}

int subtract(int i, int j)
{
    return i - j;
}

int multiply(int i, int j)
{
    return i * j;
}

int divide(int i, int j)
{
    return i / j;
}

int main()
{
    vector<int (*)(int, int)> func_vec;
    func_vec.push_back(add);
    func_vec.push_back(subtract);
    func_vec.push_back(multiply);
    func_vec.push_back(divide);

    cout << "Input two ints: " << endl;

    int input1, input2;
    cin >> input1 >> input2;

    cout << "add: " << func_vec[0](input1, input2) << endl;
    cout << "subtract: " << func_vec[1](input1, input2) << endl;
    cout << "multiply: " << func_vec[2](input1, input2) << endl;
    cout << "divide: " << func_vec[3](input1, input2) << endl;

}
