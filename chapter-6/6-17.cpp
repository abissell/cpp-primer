#include <string>
#include <iostream>
#include <cctype>

using namespace std;

bool contains_capitals(const string& str)
{
    for (auto it = str.cbegin(); it != str.cend(); ++it) {
        if (isupper(*it)) { return true; }
    }

    return false;
}

void to_all_lowercase(string& str)
{
    for (auto it = str.begin(); it != str.end(); ++it) {
        *it = tolower(*it);
    }
}

int main()
{
    string str1("a string");
    string str2("a stRiNg");
    cout << str1 << " contains capitals: " << contains_capitals(str1) << endl;
    cout << str2 << " contains capitals: " << contains_capitals(str2) << endl;

    cout << str2 << " to lowercase: ";
    to_all_lowercase(str2);
    cout << str2 << endl;

    return 0;
}
