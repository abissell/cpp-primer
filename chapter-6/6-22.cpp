#include <iostream>

using namespace std;

void swap_ptrs(int **p1, int **p2)
{
    int *tmp = *p1;
    *p1 = *p2;
    *p2 = tmp;
}

int main()
{
    int one = 1, two = 2;
    int *one_ptr = &one, *two_ptr = &two;
    swap_ptrs(&one_ptr, &two_ptr);
    cout << *one_ptr << " " << *two_ptr << endl;

    return 0;
}
