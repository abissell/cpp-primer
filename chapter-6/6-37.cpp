#include <string>

using namespace std;

string arr1[10];
string arr2[10];
string arr3[9];

string (&get_ref_to_array())[10]
{
    return arr1;
}

using str_arr = string[10];
str_arr &get_alias_ref_to_array()
{
    return arr1;
}

auto get_trailing_ref_to_array() -> string(&)[10]
{
    return arr1;
}

decltype(arr2) &get_decltype_ref_to_array()
{
    return arr1;
}

int main()
{
    string (&ref)[10] = get_ref_to_array();
    string (&alias_ref)[10] = get_alias_ref_to_array();
    string (&trailing_ref)[10] = get_trailing_ref_to_array();
    string (&decltype_ref)[10] = get_decltype_ref_to_array():
}
