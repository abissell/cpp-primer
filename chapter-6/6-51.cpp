#include <string>
#include <iostream>

using namespace std;

void f()
{
    cout << "void" << endl;
}

void f(int i)
{
    cout << "int" << endl;
}

void f(int i, int j)
{
    cout << "int, int" << endl;
}

void f(double d, double e = 3.14)
{
    cout << "double, double = 3.14" << endl;
}

int main()
{
    // f(2.56, 42); // should be ambiguous call error
    cout << "should be int: ";
    f(42);
    cout << "should be int, int: ";
    f(42, 0);
    cout << "should be double, double = 3.14: ";
    f(2.56, 3.14);
}
