#include <iostream>

using namespace std;

int sum(initializer_list<int> il)
{
    int sum = 0;
    for (const int& i : il) {
        sum += i;
    }

    return sum;
}

int main()
{
    cout << sum({1, 4, 7}) << endl;

    return 0;
}
