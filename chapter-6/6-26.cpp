#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    if (argc != 3) {
        cout << "Error: argc=" << argc << endl;
        return -1;
    }

    string result = string(argv[1]) + string(argv[2]);
    cout << result << endl;

    return 0;
}
