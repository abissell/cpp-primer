#include <string>

using namespace std;

bool str_subrange(const string& str1, const string& str2)
{
    // same sizes: return normal equality test
    if (str1.size() == str2.size()) { return str1 == str2; }

    // find the size of the smaller string
    auto size = (str1.size() < str2.size()) ? str1.size() : str2.size();

    // look at each element up to the size of the smaller string
    for (decltype(size) i = 0; i != size; ++i) {
        if (str1[i] != str2[i]) { return false; } // err #1: no return value
    }

    // err #2: control might flow off end of function
}

int main()
{
}
