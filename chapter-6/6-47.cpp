#include <string>
#include <iostream>
#include <vector>
#include <cassert>

using namespace std;

void recursively_print_vector(const vector<int>& vec,
                              vector<int>::const_iterator it)
{
    if (it != vec.cend()) {
        cout << *it++ << " ";
        recursively_print_vector(vec, it);
    } else {
        cout << endl;
    }
}

int main()
{
    vector<int> vec = {1, 2, 3, 4, 5};
#ifndef NDEBUG
    cout << "recursively printing vector of size " << vec.size() << endl;
#endif
    recursively_print_vector(vec, vec.cbegin());
}
