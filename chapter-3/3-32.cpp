#include <cstddef>
using std::size_t;

#include <iostream>
using std::cout; using std::endl;

int main()
{
    unsigned scores[11];
    unsigned grade;
    while (std::cin >> grade) {
        if (grade <= 100) {
            ++scores[grade/10];
        }
    }

    for (int i : scores) {
        cout << i << " ";
    }
    cout << endl;

    return 0;
}
