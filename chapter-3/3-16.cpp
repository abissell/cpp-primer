#include <vector>
#include <iostream>
#include <string>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
    vector<int> v1;
    cout << "size: " << v1.size() << endl;

    vector<int> v2(10);
    cout << "size: " << v2.size() << ", contents: ";
    for (int i : v2) {
        cout << i << " ";
    }
    cout << endl;

    return 0;
}
