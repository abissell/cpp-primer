#include <vector>
#include <iostream>
#include <string>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
    vector<unsigned> scores(11, 0);
    unsigned grade;
    while (cin >> grade) {
        unsigned cluster = grade / 10;
        if (cluster <= 10) {
            auto it = scores.begin();
            ++(*(it + cluster));
        }
    }

    for (unsigned i : scores) {
        cout << i << " ";
    }
    cout << endl;

    return 0;
}
