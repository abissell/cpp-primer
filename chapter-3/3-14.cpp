#include <vector>
#include <iostream>
#include <string>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
    /*
    vector<int> i_vec;
    int input;
    while (cin >> input) {
        i_vec.push_back(input);
    }

    for (int i : i_vec) {
        cout << i << " ";
    }
    cout << endl;
    */

    vector<string> str_vec;
    string input_str;
    while (cin >> input_str) {
        str_vec.push_back(input_str);
    }

    for (string s : str_vec) {
        cout << s << " ";
    }
    cout << endl;

    return 0;
}
