#include <vector>
#include <iostream>

using std::vector;

int main()
{
    vector<int> list_init = { 42, 42, 42, 42, 42, 42, 42, 42, 42, 42 };
    vector<int> constrc_init(10, 42);
    vector<int> push_init;
    for (unsigned i = 0; i < 10; ++i) {
        push_init.push_back(42);
    }

    if (list_init != constrc_init || list_init != push_init) {
        std::cerr << "some vecs not equal" << std::endl;
    }

    return 0;
}
