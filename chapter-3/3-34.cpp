#include <iostream>
using std::cout; using std::endl;

int main()
{
    int a[5] = { 1, 2, 3, 4, 5 };

    // Case where p1 and p2 point to same element, p1 unchanged
    int *p1 = &a[1], *p2 = &a[1];
    p1 += p2 - p1;
    cout << "2 == " << *p1 << endl;

    // Case where p2 > p1, p1 will be set to p2
    p2 = &a[3];
    p1 += p2 - p1;
    cout << "4 == " << *p1 << endl;

    // Case where p2 < p1, p1 will be set to p2
    p2 = &a[0];
    p1 += p2 - p1;
    cout << "1 == " << *p1 << endl;

    return 0;
}
