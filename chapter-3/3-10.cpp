#include <iostream>
#include <string>
#include <cctype>

int main()
{
    std::string input;
    getline(std::cin, input);

    for (const char &c : input) {
        if (!ispunct(c)) {
            std::cout << c;
        }
    }
    std::cout << std::endl;

    return 0;
}
