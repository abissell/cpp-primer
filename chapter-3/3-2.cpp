#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
    string line;
    // while (getline(cin, line)) { // Read a line at a time
    while (cin >> line) { // Read a word at a time
        if (!line.empty()) {
            cout << line << endl;
        }
    }
}
