#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
    string input_1, input_2;
    getline(cin, input_1);
    getline(cin, input_2);

    if (input_1 == input_2) {
        cout << "strings are equal" << endl;
    } else {
        if (input_1 > input_2) {
            cout << "first string is larger" << endl;
        } else {
            cout << "second string is larger" << endl;
        }
    }

    if (input_1.size() == input_2.size()) {
        cout << "strings have same length" << endl;
    } else {
        if (input_1.size() > input_2.size()) {
            cout << "input_1 is longer" << endl;
        } else {
            cout << "input_2 is longer" << endl;
        }
    }

    return 0;
}
