#include <iostream>
using std::cout; using std::endl;

#include <string>
using std::begin; using std::end;

int main()
{
    int a[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    int *p = begin(a);
    int *e = end(a);

    while (p != e) {
        *p = 0;
        ++p;
    }

    for (int i : a) {
        cout << i << " ";
    }
    cout << endl;

    return 0;
}
