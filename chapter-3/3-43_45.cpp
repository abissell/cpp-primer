#include <iostream>
using std::cout; using std::endl;
using std::size_t;

#include <iterator>
using std::begin; using std::end;

int main()
{
    int ia[3][4] = {
        {0, 1, 2, 3},
        {4, 5, 6, 7},
        {8, 9, 10, 11}
    };

    using row_ref = int[4];


    // Range-based for

    // Manual typing
    for (int (&row)[4] : ia) {
        for (int i : row) {
            cout << i << " ";
        }
    }
    cout << endl;

    // Type alias
    for (row_ref (&row) : ia) {
        for (int i : row) {
            cout << i << " ";
        }
    }
    cout << endl;

    // auto
    for (auto &row : ia) {
        for (int i : row) {
            cout << i << " ";
        }
    }
    cout << endl;


    // for with subscripts

    // Manual typing
    for (size_t i = 0; i != 3; ++i) {
        for (size_t j = 0; j != 4; ++j) {
            cout << ia[i][j] << " ";
        }
    }
    cout << endl;

    // Type alias
    using idx = size_t;
    for (idx i = 0; i != 3; ++i) {
        for (idx j = 0; j != 4; ++j) {
            cout << ia[i][j] << " ";
        }
    }
    cout << endl;

    // auto
    for (auto i = 0; i != 3; ++i) {
        for (auto j = 0; j != 4; ++j) {
            cout << ia[i][j] << " ";
        }
    }
    cout << endl;


    // for with pointers

    // Manual typing
    for (int (*row)[4] = begin(ia); row != end(ia); ++row) {
        for (int *i_ptr = begin(*row); i_ptr != end(*row); ++i_ptr) {
             cout << *i_ptr << " ";
        }
    }
    cout << endl;

    // Type alias
    for (row_ref *row = begin(ia); row != end(ia); ++row) {
        for (int *i_ptr = begin(*row); i_ptr != end(*row); ++i_ptr) {
            cout << *i_ptr << " ";
        }
    }
    cout << endl;

    // auto
    for (auto row = begin(ia); row != end(ia); ++row) {
        for (auto i_ptr = begin(*row); i_ptr != end(*row); ++i_ptr) {
            cout << *i_ptr << " ";
        }
    }
    cout << endl;

    return 0;
}
