#include <iostream>
#include <string>
#include <cctype>

int main()
{
    const std::string s = "Keep out!";
    for (auto &c : s) {
        c = 'J'; // compiler error
    }

    return 0;
}
