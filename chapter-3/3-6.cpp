#include <string>
#include <iostream>

int main()
{
    std::string s("A simple string");
    for (char &c : s) {
        c = 'X';
    }
    std::cout << s << std::endl;

    decltype(s.size()) i = 0;
    while (i < s.size()) {
        s[i] = 'Y';
        ++i;
    }
    std::cout << s << std::endl;

    for (i = 0; i < s.size(); ++i) {
        s[i] = 'Z';
    }
    std::cout << s << std::endl;

    return 0;
}
