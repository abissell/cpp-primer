#include <cstddef>
using std::size_t;

#include <iostream>
using std::cout; using std::endl;

int main()
{
    constexpr size_t array_size = 10;
    int i[array_size];
    for (auto idx = 0; idx <= array_size; ++idx) {
        i[idx] = idx;
    }

    for (int elem : i) {
        cout << elem << " ";
    }
    cout << endl;

    return 0;
}
