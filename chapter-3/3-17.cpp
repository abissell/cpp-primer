#include <vector>
#include <iostream>
#include <string>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
    vector<string> input_vec;
    string input;
    while (cin >> input) {
        input_vec.push_back(input);
    }

    for (string &s : input_vec) {
        for (char &c : s) {
            c = toupper(c);
        }
    }

    for (auto i = 0; i < input_vec.size(); ++i) {
        if (i % 8 == 0) {
            cout << endl;
        }
        cout << input_vec[i] << " ";
    }
    cout << endl;

    return 0;
}
